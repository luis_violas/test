# ODSOFT CA2

## Pipeline goals

1. Repository Checkout: Checkout the GIT repository;
2. War file Build the .war file and publish it on Jenkins;
3. Javadoc Build the Javadoc and publish it on Jenkins;
4. Unit Tests Execute the Unit Tests, generate a Unit Tests Report, generate a Unit Tests Coverage Report and publish
   both reports on Jenkins;
5. Integration Tests Execute the Integration Tests, generate an Integration Tests Report, generate an Integration Tests
   Coverage Report and publish both reports onJenkins;
6. Mutation Tests Execute the Mutation Tests, generate a Mutation Coverage Report and publish it on Jenkins;
7. System Test Deploy the application (.war file) to a pre-configured Tomcat Server instance. Perform an automatic smoke
   test. This smoke test can be as simple as using curl to check if the base url of the application is responsive after
   deployment in the Tomcat Server, ensuring that the application is properly deployed to the Staging Environment;
8. UI Acceptance Manual Tests A user should be notified by email of the successful execution of all the previous tests
   and be asked to perform a manual test. In order to cancel the progression or proceed, a UI Acceptance Manual Test
   must take place. The pipeline should wait for a user manual confirmation on Jenkins;
9. Continuous Integration Feedback Push a tag to the repository with the Jenkins build number and status (e.g.
   Build#32-Passed or Build#32-Failed).

## Pipeline design

Two pipelines were designed, one with sequential steps and the other one with parallel steps.

### Sequential

For the sequential pipeline, the following step order was defined:

![Parallel Pipeline Sketch](documents_ca2/sequential-diagram.png)

1. Git Checkout - The first step is to obtain the latest version of the source code from the git repository.
2. Build - After checkout, the next step is to run ./gradlew clean build', in order to clean the workspace 
3. War File - After the build, the war file is deployed 
4. Unit Testing	 - For this step, the Gradle tasks for the execution of the unit tests.
5. Integration Testing - This tests should run after successfully running the units tests, using the previously compiled files (in unit tests).
6. Generate Jacoco report - After their successful run of the Unit and Integration Tests, jacoco report file is generated.
7. Mutation Tests - Similar to the integrations test step this tests require the compiled files from unit tests and
   should only be run after unit tests. This way we optimize the pipeline computational resources by not re-running the
   unit tests (mutation tests depends on unit tests).
8. Javadoc - This task is intended to run after all tests successfully execute as there is no point in generating
   javadoc for a unstable build.
9. UI Acceptance - The step sends an email and asks a user to perform a manual test after all the previously steps are successful and waits for the manual validation from the notified user that triggers the next step.
10. Continuous Integration - This final step is dependant on the success, or not, of all the previous steps.

### Parallel

This pipeline was designed based on the sequential pipeline that was described above.

![Parallel Pipeline Sketch](documents_ca2/parallel-diagram.png)

After the repository checkout the steps "War File", "Javadoc", "Unit tests" and "Integration Tests" are ran in parallel.
No more steps run in parallel since all the missing steps require the artifacts and results generated previously.

### Component 1 - Sequential Pipeline with Build Pipeline plugin

For this component the "Build Pipeline" plugin was chosen over the "Build Promotions" since it is less complex to
configure and more straightforward to visualize the pipeline execution.

Each step mentioned in the diagram was implemented in a Jenkins job.


### Component 2 - Parallel Pipeline with Build Pipeline plugin


### Component 3 - Scripted Sequential Pipeline


### Component 4 - Scripted Parallel Pipeline

This component was based in the component 3 implementation. To fork the stages that have to run in parallel a multi
stage was created, inside it a parallel directive was used.

## Jenkins required plugin list

- All the recommended plugins
- JaCoCo
- PIT Mutation
- Javadoc
- Promoted builds
- Build pipeline

## Analysis



### Setup



when setting up the project locally there are a few things that may be needed, such as: change Java compiler version to 1.8, GWT version and Gradle.
To run locally:
- gradlew build
This command creates the /build path but some folder may be missing. And for that it can be necessary to run more commands, such as:
- gradlew war
This creates the war folder in /build/war path.



### Running Tests



For running Unit tests there is a task in build.gradle called "test" that when running >gradlew test.
This generates in path /build/reports/test the html report for junit.



For running Integration tests there is a task in build.gradle called "test" that when running >gradlew integrationTest.
This generates in path /build/reports/interationTest the html report for junit.



# Problem when running the integration test



When first trying to run this tests, this problem appeared:
- pt.isep.cms.contacts.client.ExampleGWTTest > testContactSort FAILED
com.google.gwt.core.shared.SerializableThrowable



pt.isep.cms.contacts.client.ExampleGWTTest > testContactsService FAILED
com.google.gwt.junit.client.TimeoutException



2 tests completed, 2 failed
:integrationTest FAILED



Fixed this by: following this tutorial https://examples.javacodegeeks.com/core-java/gradle/gradle-gwt-integration-example/ and making changes to TestCMSJUnit.gwt.xml



# Mutation Tests
For running Mutation tests there is a task in build.gradle called "test" that when running >gradlew pitest.
This generates in path /build/reports/pitest the html report for mutation test.



In build.gradle it was needed to use the pitest task in gradle.build and the plugin 'info.solidsoft.pitest'. Then create a pitest task:



- pitest {
targetClasses = ['pt.isep.cms.*']
threads = 4
outputFormats = ['HTML', 'XML'] //genates both html and xml
timestampedReports = false
}



# Deploy of war file
Needed to install Deploy to container Plugin on jenkins.



In the script give the url to tomcat and it was necessary to change tomcat�s port because it�s by default the same as jenkins. So, changed it to port=8888.



Also in order to be able to successfully run tomcat it�s necessary to create users in tomcat/conf/tomcat-users.xml




# Email
necessary to install plugin Email Extension Plugin.



For the emails to be easily managed, created an email address:



odsoft208@gmail.com


pass: Catarina@ODSOFT208


Email relevant info:


body message: Hello! You need to check the output and provide approval/denial to proceed
subject: $PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!



# Create tag in jenkins



For giving feedback to git, in Jenkinsfile created a stage for pushing a tag regardig build number and current status of all previous stages.



Build #${currentBuild.number}-${currentBuild.currentResult}




## Execution times



| Pipeline Type | Mode | Exec. Time |
| ------------- | ---------- | ---------- |
| Scripted | Sequential | 2m : 12s |
| Scripted | Parallel | 1m : 40s |